const express = require('express')
const cors = require('cors')
const app = express()
const path = require('path');

const PORT = 8000;

const frontendServerUrl = "http://localhost:3000"

app.use(cors({
    origin: frontendServerUrl
}))

app.get("/", (req, res, next) => {
    res.status(200)
    res.send();
})

app.get("/font", (req, res, next) => {
    var options = {
        root: path.resolve(__dirname, 'public'),
    }

    res.sendFile("example.ttf", options, (err) => err ? console.error(err) : null)
})

app.listen(PORT, () => {
    console.log(`server listening on http://localhost:${PORT}`);
})
