
const API_URL = 'http://localhost:8000';

export const getFont = () => {
  return fetch(API_URL + '/font')
          .then(res => res.arrayBuffer())
}

export const sendFile = (file) => {
	return fetch(API_URL + '/file', {
		method: 'POST',
		body: file
	});
}
