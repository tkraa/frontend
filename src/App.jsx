// import './App.css';
import React, { useEffect, useState, useRef } from 'react'
import * as api from './api'
import styles from './App.module.css'
import cells from './cells.svg'

import { FileUploader } from './components/FileUploader'
  
function App() {

  const canvasRef = useRef({})
  const textareaRef = useRef({})
  const [lines, setLines] = useState([])

	const sendFile = (file) => {

	}

  const saveFont = (font, name) => {
    const fontFace = new FontFace(name, font);
    document.fonts.add(fontFace);
    localStorage.setItem('font', font)
  }

  useEffect(() => {
    api
      .getFont()
      .then(font => saveFont(font, "custom-font"))
      .catch(console.error)
  })

  const onInput = (ev) => {
    if (ev.nativeEvent.inputType == "insertLineBreak") {
      // console.log('enter')
    }
    setLines(textareaRef.current.value.split('\n'))
		console.log(lines)
  }

  return (
    <div className={styles.App}>

      {/* Header */}
      <div className={styles.header}>
        <div><span className={styles.headerText}>REVENU</span></div>
        <div><span className={styles.headerText}>v.1</span></div>
      </div>

      {/* Main section */}
      <div className={styles.contentWrapper}>

        <div className={styles.inputWrapper}>

          {/* User Input */}
          <textarea onInput={onInput} ref={textareaRef} className={styles.textarea} placeholder="Напишите текст"></textarea>  
          

          {/* Загрузите свой шрифт */}
          <div>Загрузите пример своего почерка</div>
          <FileUploader onFileLoaded={api.sendFile} label={"Upload"} />
        </div>

        <div className={styles.outputWrapper}>
          {/* User Output */}
          <div ref={canvasRef} className={styles.output} styles={{'font-style': 'custom-font'}}>
            {
              lines
								.map(line => 
										<React.Fragment>
											<pre key={line}>{line}</pre>
										</React.Fragment>
								)
            }
          </div>
        </div>

      </div>
    </div>
  );
}

export default App;
