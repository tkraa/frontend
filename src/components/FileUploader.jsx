import React, {useState} from 'react';
import styles from './FileUploader.module.css'

export const FileUploader = (props) => {

  const [fileName, setFileName] = useState('')
  const [file, setFile] = useState({})

  const handleOnChange = (ev) => {
    setFile(ev.target.files[0])
		props.onFileLoaded(ev.target.files[0])
    setFileName(ev.target.files[0]?.name)
  }

  return (
    <React.Fragment>
      <label className={styles.uploadFileLabel} htmlFor="font-file">{props.label}</label>
      <input id="font-file" className={styles.uploadFile} type='file' onChange={handleOnChange} />
      <span>{fileName}</span>
    </React.Fragment>)
}
