"use strict"
var input = document.getElementById("textarea")
var debug = document.getElementById("debug")
var canvas = document.getElementById("canvas")
var context = canvas.getContext('2d')

// context.font = 	"28px custom-font"

var lines = [];

input.addEventListener('keyup', (event) => {
  lines = event.target.value.split('\n')
  
  rerenderText(lines);
})

debug.onclick = () => {
  console.log(lines);
}

window.onload = () => {
}